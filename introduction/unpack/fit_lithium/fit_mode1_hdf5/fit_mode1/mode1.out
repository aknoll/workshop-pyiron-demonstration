 -------------------------------------------------------------
 ---------------------- Welcome to the -----------------------
     RuNNer Neural Network Energy Representation - RuNNer     
 ----------  (c) 2008-2022 Prof. Dr. Joerg Behler   ----------
 ----------  Georg-August-Universitaet Goettingen   ----------
 ----------           Theoretische Chemie           ----------
 ----------              Tammannstr. 6              ----------
 ----------        37077 Goettingen, Germany        ----------
 -------------------------------------------------------------
 -------------------------------------------------------------
  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the 
  Free Software Foundation, either version 3 of the License, or 
  (at your option) any later version.
    
  This program is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
  for more details.
      
  You should have received a copy of the GNU General Public License along
  with this program. If not, see http://www.gnu.org/licenses. 
 -------------------------------------------------------------
 -------------------------------------------------------------
 When using RuNNer, please cite the following papers:
 J. Behler, Angew. Chem. Int. Ed. 56, 12828 (2017).
 J. Behler, Int. J. Quant. Chem. 115, 1032 (2015).
 -------------------------------------------------------------
 Whenever using high-dimensional NNPs irrespective of the Code please cite:
 J. Behler and M. Parrinello, Phys. Rev. Lett. 98, 146401 (2007).
 -------------------------------------------------------------
 The reference for the atom-centered symmetry functions is:
 J. Behler, J. Chem. Phys. 134, 074106 (2011).
 -------------------------------------------------------------
 For high-dimensional NNs including electrostatics:
 N. Artrith, T. Morawietz, and J. Behler, Phys. Rev. B 83, 153101 (2011).
 -------------------------------------------------------------
 -------------------------------------------------------------
 RuNNer has been written by Joerg Behler
  
 *** with contributions from some friends ***
  
 Tobias Morawietz - Nguyen Widrow weights and electrostatic screening
 Jovan Jose Kochumannil Varghese - Pair symmetry functions and pair NNPs
 Michael Gastegger and Philipp Marquetand - element decoupled Kalman filter
 Andreas Singraber - more efficient symmetry function type 3 implementation
 Sinja Klees and Mariana Rossi - some infrastructure for i-Pi compatibility
 Emir Kocer - Symmetry function groups
 Fenfei Wei and Emir Kocer - Hessian, frequencies and normal modes
 Alexander Knoll - vdW corrections long ranged
 Marco Eckhoff - spin-dependent atom-centered symmetry functions
 -------------------------------------------------------------
 -------------------------------------------------------------
 General job information:
 -------------------------------------------------------------
 Executing host    : lap2p               
 User name         : kalm                
 Starting date     :  1. 6.2022
 Starting time     : 13 h  4 min 
 Working directory :                                                             
 -------------------------------------------------------------
 -------------------------------------------------------------
 Serial run requested
 -------------------------------------------------------------
 Reading control parameters from input.nn
 =============================================================
 -------------------------------------------------------------
 =============================================================
 General input parameters:
 -------------------------------------------------------------
 NNP generation                                           2
 Short range NN is on
 Electrostatic NN is off
 vdW corrections switched off
 -------------------------------------------------------------
 RuNNer nn_type_short                                     1
 RuNNer is started in mode for symmetry function calculation (1)
 debugging mode is                                       F
 parallelization mode                                     1
 enable detailed time measurement                        F
 using symmetry function groups                          F
 silent mode                                             F
 number of elements                                       1
 elements (sorted):
  3 Li
 seed for random number generator                       975
 random number generator type                             5
 remove free atom reference energies                     F
 remove vdw dispersion energy and forces                 F
 shortest allowed bond in structure                   0.500
 Cutoff_type for symmetry function is                     1
 Cutoff_alpha for inner cutoff radius is              0.000
 -------------------------------------------------------------
 Parameters for symmetry function generation: short range part:
 -------------------------------------------------------------
 using forces for fitting                                T
 using atomic energies for fitting                       F
 -------------------------------------------------------------
 percentage of data for testing (%)                 10.0000
 =============================================================
 Element pairs:   1    , shortest distance (Bohr)
 pair    1 Li Li      3.164
 =============================================================
 -------------------------------------------------------------
 -------------------------------------------------------------
 -------------------------------------------------------------
  short range atomic symmetry functions element Li :
 -------------------------------------------------------------
    1 Li  2  Li                 0.000000   0.000000  12.000000
    2 Li  2  Li                 0.004675   0.000000  12.000000
    3 Li  2  Li                 0.010843   0.000000  12.000000
    4 Li  2  Li                 0.019394   0.000000  12.000000
    5 Li  2  Li                 0.031930   0.000000  12.000000
    6 Li  2  Li                 0.051599   0.000000  12.000000
    7 Li  3  Li Li   0.000000  -1.000000   1.000000  12.000000
    8 Li  3  Li Li   0.000000   1.000000   1.000000  12.000000
    9 Li  3  Li Li   0.000000  -1.000000   2.000000  12.000000
   10 Li  3  Li Li   0.000000   1.000000   2.000000  12.000000
   11 Li  3  Li Li   0.000000  -1.000000   4.000000  12.000000
   12 Li  3  Li Li   0.000000   1.000000   4.000000  12.000000
   13 Li  3  Li Li   0.000000  -1.000000   8.000000  12.000000
   14 Li  3  Li Li   0.000000   1.000000   8.000000  12.000000
 -------------------------------------------------------------
 Maximum number of atoms:           54
 -------------------------------------------------------------
 Calculating Symmetry Functions
 for          176  structures
 -------------------------------------------------------------
           1  Point is used for testing            1
           2  Point is used for training            1
           3  Point is used for training            2
           4  Point is used for training            3
           5  Point is used for training            4
           6  Point is used for training            5
           7  Point is used for training            6
           8  Point is used for training            7
           9  Point is used for training            8
          10  Point is used for training            9
          11  Point is used for training           10
          12  Point is used for training           11
          13  Point is used for training           12
          14  Point is used for training           13
          15  Point is used for training           14
          16  Point is used for training           15
          17  Point is used for training           16
          18  Point is used for training           17
          19  Point is used for training           18
          20  Point is used for training           19
          21  Point is used for testing            2
          22  Point is used for training           20
          23  Point is used for training           21
          24  Point is used for training           22
          25  Point is used for testing            3
          26  Point is used for training           23
          27  Point is used for training           24
          28  Point is used for training           25
          29  Point is used for training           26
          30  Point is used for training           27
          31  Point is used for training           28
          32  Point is used for training           29
          33  Point is used for training           30
          34  Point is used for training           31
          35  Point is used for training           32
          36  Point is used for testing            4
          37  Point is used for training           33
          38  Point is used for training           34
          39  Point is used for training           35
          40  Point is used for training           36
          41  Point is used for training           37
          42  Point is used for training           38
          43  Point is used for training           39
          44  Point is used for training           40
          45  Point is used for training           41
          46  Point is used for training           42
          47  Point is used for training           43
          48  Point is used for training           44
          49  Point is used for training           45
          50  Point is used for training           46
          51  Point is used for training           47
          52  Point is used for training           48
          53  Point is used for training           49
          54  Point is used for testing            5
          55  Point is used for training           50
          56  Point is used for training           51
          57  Point is used for training           52
          58  Point is used for training           53
          59  Point is used for training           54
          60  Point is used for testing            6
          61  Point is used for testing            7
          62  Point is used for training           55
          63  Point is used for training           56
          64  Point is used for testing            8
          65  Point is used for training           57
          66  Point is used for training           58
          67  Point is used for training           59
          68  Point is used for training           60
          69  Point is used for testing            9
          70  Point is used for training           61
          71  Point is used for training           62
          72  Point is used for training           63
          73  Point is used for training           64
          74  Point is used for training           65
          75  Point is used for training           66
          76  Point is used for training           67
          77  Point is used for training           68
          78  Point is used for training           69
          79  Point is used for training           70
          80  Point is used for training           71
          81  Point is used for training           72
          82  Point is used for training           73
          83  Point is used for training           74
          84  Point is used for training           75
          85  Point is used for training           76
          86  Point is used for training           77
          87  Point is used for training           78
          88  Point is used for training           79
          89  Point is used for training           80
          90  Point is used for training           81
          91  Point is used for training           82
          92  Point is used for training           83
          93  Point is used for training           84
          94  Point is used for training           85
          95  Point is used for training           86
          96  Point is used for training           87
          97  Point is used for training           88
          98  Point is used for training           89
          99  Point is used for training           90
         100  Point is used for testing           10
         101  Point is used for training           91
         102  Point is used for training           92
         103  Point is used for testing           11
         104  Point is used for training           93
         105  Point is used for training           94
         106  Point is used for training           95
         107  Point is used for training           96
         108  Point is used for training           97
         109  Point is used for testing           12
         110  Point is used for testing           13
         111  Point is used for training           98
         112  Point is used for training           99
         113  Point is used for training          100
         114  Point is used for training          101
         115  Point is used for training          102
         116  Point is used for training          103
         117  Point is used for training          104
         118  Point is used for training          105
         119  Point is used for training          106
         120  Point is used for training          107
         121  Point is used for training          108
         122  Point is used for training          109
         123  Point is used for training          110
         124  Point is used for training          111
         125  Point is used for training          112
         126  Point is used for testing           14
         127  Point is used for training          113
         128  Point is used for training          114
         129  Point is used for training          115
         130  Point is used for training          116
         131  Point is used for training          117
         132  Point is used for training          118
         133  Point is used for training          119
         134  Point is used for training          120
         135  Point is used for training          121
         136  Point is used for testing           15
         137  Point is used for training          122
         138  Point is used for training          123
         139  Point is used for training          124
         140  Point is used for training          125
         141  Point is used for training          126
         142  Point is used for training          127
         143  Point is used for training          128
         144  Point is used for training          129
         145  Point is used for training          130
         146  Point is used for training          131
         147  Point is used for training          132
         148  Point is used for training          133
         149  Point is used for training          134
         150  Point is used for training          135
         151  Point is used for training          136
         152  Point is used for testing           16
         153  Point is used for training          137
         154  Point is used for training          138
         155  Point is used for training          139
         156  Point is used for training          140
         157  Point is used for training          141
         158  Point is used for training          142
         159  Point is used for testing           17
         160  Point is used for training          143
         161  Point is used for training          144
         162  Point is used for training          145
         163  Point is used for training          146
         164  Point is used for training          147
         165  Point is used for training          148
         166  Point is used for training          149
         167  Point is used for training          150
         168  Point is used for training          151
         169  Point is used for training          152
         170  Point is used for testing           18
         171  Point is used for training          153
         172  Point is used for training          154
         173  Point is used for training          155
         174  Point is used for training          156
         175  Point is used for training          157
         176  Point is used for training          158
 -------------------------------------------------------------
 Number of fitting points:          158
 Number of testing points:           18
 Number of rejected points:           0
 -------------------------------------------------------------
 Total runtime (s)  :          5.683
 Total runtime (min):          0.095
 Total runtime (h)  :          0.002
 Normal termination of RuNNer
 -------------------------------------------------------------
